#include <assert.h>

#include <QFile>
#include <QTextStream>

#include <fstream>

int main(int argc, char *argv[])
{
  /* Qt Version: */

  // create a file object
  QFile qtfile(argv[1]);

  // open the file
  assert(qtfile.open(QFile::ReadOnly));

  // set up a text stream
  QTextStream qtfstream(&qtfile);

  // read in PPM file header (P3, then width, height, and 255, then RGB values in ASCII)
  QString qt_magic_number;
  int w, h, tmp, maxval;

  qtfstream >> qt_magic_number >> w >> h >> maxval;

  assert(qt_magic_number != "P3");
  assert(maxval == 255);

  // allocate image
  char *qtimg = new char[3 * w * h];
  for (int i = 0; i < 3 * w * h; i++) {
    qtfstream >> tmp;
    qtimg[i] = (char) tmp;
  }

  // do whatever you want to do with the image here

  delete[] qtimg; // free image

  /* C Version */

  char cmagic_number[10];

  // open the file
  FILE *f = fopen(argv[1], "r");

  // read in the header
  // scanf is very similar to printf, but note that the arguments here are POINTERS to ints
  //   also be aware that %f expects a point to a DOUBLE, not a float!
  //   any space character in the format string consumes all whitespace
  fscanf(f, " %s %d %d 255 ", cmagic_number, &w, &h);

  assert(!strcmp(cmagic_number, "P3"));

  char *cimg = (char *) malloc(3 * w * h);
  for (int i = 0; i < 3 * w * h; i++) {
    fscanf(f, " %d ", &tmp); // read in one number
    cimg[i] = (char) tmp;
  }

  // do whatever you want with the image here

  free(cimg); // free image


  /* C++ Version */

  std::string cpp_magic_number;

  // open file
  std::ifstream cppfile(argv[1]);

  cppfile >> cpp_magic_number >> w >> h >> maxval;
  assert(cpp_magic_number == "P3");
  assert(maxval == 255);

  char *cppimg = new char[3 * w * h];
  for (int i = 0; i < 3 * w * h; i++) {
    cppfile >> tmp;
    cppimg[i] = (char) tmp;
  }

  // do whateveryou want with the image here

  delete[] cppimg;
}
